..
    :copyright: Copyright (c) 2014 ftrack

********
Glossary
********

.. glossary::

    API
        Application programming interface.

    ftrack
        Cloud based Creative Project management tool for Production Tracking,
        Asset Management and Team Collaboration.

        .. seealso::

            `www.ftrack.com <https://www.ftrack.com>`_

    hook
        Used to extend or modify the default behaviour in ftrack connect. See
        :ref:`developing/hooks` for more information.

    Houdini
        Houdini is a 3D animation application software developed by
        Side Effects Software. Visit the website at http://www.sidefx.com/

    JSON
        (JavaScript Object Notation), a lightweight data-interchange format.
        Visit the website at http://www.json.org/

    mp4
        MPEG-4 Part 14 or MP4 is a digital multimedia format most commonly used
        to store video and audio, but can also be used to store other data such
        as subtitles and still images. It's being developed by International
        Organization for Standardization (ISO). Visit the website at
        http://www.iso.org/

    python
        A programming language that lets you work more quickly and integrate
        your systems more effectively. Often used in creative industries. Visit
        the language website at http://www.python.org

    service context menu
        The service provides a context menu for performing basic actions. It
        is accessible by :kbd:`clicking` on the service icon.

        .. image:: /image/service_menu.png

    webm
        WebM is a video file format intended primarily for royalty-free use in
        the HTML5 video tag. Visit the website at http://www.webmproject.org/
